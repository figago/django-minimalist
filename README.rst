Overview
========

This is a simple really hello world using django.
We will be using Django 1.4.1 , so it should be installed on your system.

Thus, we will NOT need:
a database installed.
the ORM.
the cache system.

We WILL use:
the url routing rules.
the template system (at least for serving the index.html).
the staticfiles system (at least to load one js script).

A bit of Django vocabulary
==========================

the 'web site' is called the 'Django project'

A Django project contains at least one 'Django app', usually it contains several 'Django apps'.

Each Django app is itself a web application.
It has its own templates and static files, located in myapp/templates, myapp/static, respectively.
(Other paths for templates and static can be added, refer to settings.py or the documentation for more details.)

Correctly combined, several nice django apps build up a great django project.
A django app is a python package that can be imported just as any other python package.

An app can be used in several projects.


Getting started
===============

Create the project:
-------------------

So let's create our project (you can use more informative names instead of myproject/site/..)::

mkdir myproject
cd myproject
django-admin.py startproject mysite

this command lays the structure of the project creating an initial Django app called mysite and containg a number of files.

have a look at the settingsa::

    vim mysite/mysite/settings.py

As we are not using a database we will just comment out the DATABASES setting.

We will comment out everything not needed in INSTALLED_APPS
keeping only 'django.contrib.staticfiles' which is very handy handling the static files (it was not the case before 1.3)

Create the views in an app:
---------------------------
Django creates a first default app (mysite here), we will just add a file views.py to it.

Now create a views.py in mysite/mysite
we will have our views there

Add some url patterns to mysite/mysite/urls.py

Now for django to be fully aware of the new django app (mysite), we need to add it to INSTALLED_APPS in settings.py
It allows django to find templates and static files automatically when located in the paths mentioned above.

Run the server::
---------------
python manage.py runserver

And there you are up and running minimal web service.

Beyond helloworld
=================
Adding apps:
------------
A django project can easily grow by adding django apps (python packages in mysite/).

Adding a database or databases (an SQL and a nonrel):
-----------------------------------------------------
If a database is needed later on, uncomment out DATABASES, choose a backend, define your models
and get the django ORM do the work for you.
For now most SQL backends are supported (Postgres and Sqlite even support Python3 in the dev version).
