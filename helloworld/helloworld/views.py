from django.http import HttpResponse
from django.utils import simplejson as json
from django.shortcuts import render_to_response

def index(request):
    return render_to_response("helloworld/index.html")

def jhelloworld(request, name="Foo"):
    """Return a json with the name of the user."""
    user_info = {'name': name}
    return HttpResponse(json.dumps(user_info), mimetype="application/json")
